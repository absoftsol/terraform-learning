
provider "aws" {
  region = "us-west-2"
#  access_key = "AKIA27GGASCDH2MUZJ53"
#  secret_key = "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"
}

variable vpc_cidr_block{}
variable subnet_cidr_block{}
variable az_region{}
variable env_name {}

resource "aws_vpc" "my-tf-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    "Name" = "${var.env_name}-vpc"  }
}

resource "aws_subnet" "tf-subnet1" {
  vpc_id = aws_vpc.my-tf-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.az_region

  tags = {
    "Name" = "${var.env_name}-subnet1"
  }  
}